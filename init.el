;;; init.el --- user-init-file                    -*- lexical-binding: t -*-
;;; Commentary:
;;; Code:
(progn ;     startup
  (defvar before-user-init-time (current-time)
    "Value of `current-time' when Emacs begins loading `user-init-file'.")
  (message "Loading Emacs...done (%.3fs)"
           (float-time (time-subtract before-user-init-time
                                      before-init-time)))
  ;; Make settings to improve startup time
  (defvar ang/file-name-handler-alist file-name-handler-alist)
  (setq gc-cons-threshold most-positive-fixnum
        gc-cons-percentage 0.6
        file-name-handler-alist nil)
  ;; Reset the settings after init
  (add-hook 'after-init-hook
            (lambda ()
              (setq gc-cons-threshold 16777216
                    gc-cons-percentage 0.1)
              file-name-handler-alist ang/file-name-handler-alist))
  ;; Show information about init.el load time after finishing
  (add-hook 'after-init-hook
            (lambda ()
              (message
               "Loading %s...done (%.3fs) [after-init]" user-init-file
               (float-time (time-subtract (current-time)
                                          before-user-init-time))))
            t)
  (setq user-init-file (or load-file-name buffer-file-name))
  (setq user-emacs-directory (file-name-directory user-init-file))
  (message "Loading %s..." user-init-file)
  (setq package-enable-at-startup nil)
  (setq inhibit-startup-buffer-menu t)
  (setq inhibit-startup-screen t)
  (setq inhibit-startup-echo-area-message "angerborn")
  (setq initial-buffer-choice t)
  (setq initial-scratch-message "")
  (setq load-prefer-newer t)
  (scroll-bar-mode 0)
  (tool-bar-mode 0)
  (menu-bar-mode 0))

;; Bootstrap and init straight.el
(defvar bootstrap-version)
(let ((bootstrap-file
       (expand-file-name "straight/repos/straight.el/bootstrap.el" user-emacs-directory))
      (bootstrap-version 5))
  (unless (file-exists-p bootstrap-file)
    (with-current-buffer
        (url-retrieve-synchronously
         "https://raw.githubusercontent.com/raxod502/straight.el/develop/install.el"
         'silent 'inhibit-cookies)
      (goto-char (point-max))
      (eval-print-last-sexp)))
  (load bootstrap-file nil 'nomessage))
;; Custom elisp lives in /elisp
(add-to-list 'load-path (expand-file-name "elisp/" user-emacs-directory))

(progn ;    `use-package'
  (straight-use-package 'use-package)
  (require 'use-package)
  (setq-default straight-use-package-by-default t))

(use-package no-littering
  :config
  ;; These aren't necessarily configurations of no-littering,
  ;; but also suggestions from this package.
  (setq auto-save-file-name-transforms
        `((".*" ,(no-littering-expand-var-file-name "auto-save/") t)))
  (setq custom-file (no-littering-expand-etc-file-name "custom.el"))
  (if (file-exists-p custom-file) (load custom-file)))

(use-package dash
  :config (dash-enable-font-lock))

(use-package tramp
  :straight nil
  :defer t
  :config
  ;; Use the path of the remote machine
  (add-to-list 'tramp-remote-path 'tramp-own-remote-path)
  (add-to-list 'tramp-default-proxies-alist '(nil "\\`root\\'" "/ssh:%h:"))
  (add-to-list 'tramp-default-proxies-alist '("localhost" nil nil))
  (add-to-list 'tramp-default-proxies-alist
               (list (regexp-quote (system-name)) nil nil))
  (setq tramp-backup-directory-alist
        (list (cons "." (no-littering-expand-var-file-name "tramp/backup/")))))

(use-package recentf
  :straight nil
  :config
  (add-to-list 'recentf-exclude "^/\\(?:ssh\\|su\\|sudo\\)?:")
  (add-to-list 'recentf-exclude no-littering-var-directory)
  (add-to-list 'recentf-exclude no-littering-etc-directory)
  ;; I'm a heavy user of recent files
  (setq-default recentf-max-saved-items 1000))

;; Diminish all the modes
(use-package diminish :defer t)
(use-package abbrev :straight nil :diminish abbrev-mode)
(use-package autorevert :straight nil :diminish auto-revert-mode)
(use-package outline :straight nil :diminish outline-minor-mode)
(use-package reveal :straight nil :diminish reveal-mode)
(use-package eldoc
  :straight nil
  :diminish eldoc-mode
  :config (global-eldoc-mode))

(use-package general :config (general-override-mode))
(general-create-definer general-spc
  :states 'normal
  :keymaps 'override
  :prefix "SPC"
  :non-normal-prefix "C-SPC")
;; SPC u for universal argument.
(general-spc "u" #'universal-argument)

(use-package evil
  :init
  (setq evil-want-keybinding nil)
  ;; Sometimes I want to actually use the prefix argument but in those cases it's
  ;; probably easier to just use a number instead. Such as `1 M-x compile` instead
  ;; of using `C-u M-x compile`. Also makes it possible for me to keep C-u to scroll.
  (setq evil-want-C-u-scroll t)
  (evil-mode 1))

(use-package evil-collection
  :after evil
  :config (evil-collection-init))

(use-package evil-commentary
  :diminish evil-commentary-mode
  :after evil
  :config (evil-commentary-mode))

(use-package evil-org
  :after evil
  :config
  (add-hook 'evil-org-mode-hook
            (lambda ()
              (evil-org-set-key-theme)))
  (require 'evil-org-agenda)
  (evil-org-agenda-set-keys))

(use-package evil-magit
  :after magit)

(use-package magit
  :defer t
  :hook (magit-process-mode . goto-address-mode)
  :general
  (general-spc
    "g" '(nil :which-key "git")
    "gb" #'magit-blame
    "gd" #'magit-diff-buffer-file
    "gf" #'magit-file-dispatch
    "gl" #'magit-log-buffer-file
    "gg" #'magit-status)
  :config
  (magit-add-section-hook 'magit-status-sections-hook
                          'magit-insert-modules
                          'magit-insert-stashes
                          'append)
  (put 'magit-edit-line-commit 'disabled nil)
  (put 'magit-diff-edit-hunk-commit 'disabled nil)
  (magit-auto-revert-mode))

(use-package keychain-environment
  :after magit
  :config (add-hook 'magit-credential-hook #'keychain-refresh-environment))

(use-package flycheck
  :diminish flycheck-mode
  :hook (prog-mode . flycheck-mode))

(use-package smart-jump
  :config (smart-jump-setup-default-registers))

(use-package wgrep
  :defer t)

(use-package avy
  :general (:keymaps 'override :states 'normal "C-s" 'avy-goto-char-timer))

(use-package link-hint :general (general-spc "fl" #'link-hint-open-link))

(use-package ace-window
  :general (general-spc "w" #'ace-window))

(use-package eyebrowse
  :config
  (setq eyebrowse-new-workspace t)
  (setq eyebrowse-mode-line-separator " ")
  (eyebrowse-mode)
  (eyebrowse-setup-opinionated-keys t)
  )

(use-package deadgrep
  :general (general-spc "fd" #'deadgrep))

(use-package projectile
  :init
  (general-spc "p" '(:keymap projectile-command-map :package projectile :which-key "projectile"))
  :commands 'projectile-project-p
  :diminish projectile-mode
  :config
  ;; Use rg --files instead, it's faster and doesn't need to iterate
  ;; submodules. It also respects .ignore files.
  (setq projectile-git-command "rg --files -0")
  (setq projectile-git-submodule-command nil)
  (setq projectile-completion-system 'ivy)
  (projectile-mode))

(use-package counsel-projectile
  :general (general-spc "fp" #'counsel-projectile-rg)
  :config (counsel-projectile-mode))

(use-package which-key
  :diminish which-key-mode
  :config (which-key-mode))

(use-package undo-tree
  :diminish undo-tree-mode
  :config
  (setq undo-tree-visualizer-timestamps t)
  (global-undo-tree-mode))

(use-package company
  :diminish company-mode
  :config
  (setq
   ;; No delay before completion
   company-idle-delay 0.2
   ;; Start completion after 2 characters
   company-minimum-prefix-length 2
   ;; Don't downcase completions (why would you do that!?)
   company-dabbrev-downcase nil)
  (global-company-mode))


(defun my-buffer-switch ()
  "Switch buffer using projectile if in a project, otherwise choose from all."
  (interactive)
  (if (projectile-project-p) (projectile-switch-to-buffer) (ivy-switch-buffer)))

(defun my-find-file ()
  "Find file using projectile if in a project, otherwise normally."
  (interactive)
  (if (projectile-project-p) (projectile-find-file) (counsel-find-file)))

(use-package ivy
  :diminish ivy-mode
  ;; Exit ivy mini-buffers with esc
  :bind (:map ivy-minibuffer-map ("<escape>" . 'minibuffer-keyboard-quit))
  :general
  (general-spc
   "r" #'ivy-resume
   "`" #'evil-switch-to-windows-last-buffer
   "," #'my-buffer-switch
   "<" #'ivy-switch-buffer
   "SPC" #'my-find-file
   "." #'counsel-find-file)
  :config
  (setq ivy-count-format "(%d/%d) ")
  (setq ivy-use-selectable-prompt t)
  (setq ivy-use-virtual-buffers t)
  (setq ivy-height 20)
  (setq ivy-display-style 'fancy)
  (ivy-mode 1))

(defun my/kill-this-buffer ()
  "Stable version of 'kill-this-buffer'."
  (interactive)
  (kill-buffer (current-buffer)))

(general-spc
  "b" '(nil :which-key "buffers")
  "bb" #'ivy-switch-buffer
  "bk" #'my/kill-this-buffer
  "br" #'revert-buffer)

(use-package hydra
  :defer t)

(use-package ivy-hydra
  :after ivy)

(use-package counsel
  :diminish counsel-mode
  :config
  ;; Truncate lines longer than 250 characters so that it doesn't slow down emacs.
  ;; Stolen from https://oremacs.com/2018/03/05/grep-exclude/
  (setq counsel-rg-base-command "rg -i -M 250 --no-heading --line-number --color never %s .")
  (setq counsel-git-cmd "rg --files")
  (general-def :keymaps 'override :states 'normal "C-f" 'counsel-grep-or-swiper)
  (general-spc
   "fr" #'counsel-rg)
  (counsel-mode 1))

(use-package smex :defer t)
(use-package flx :defer t)

(use-package yasnippet
  :config
  ;; Enter insert mode when expanding snippet
  (add-hook 'yas-before-expand-snippet-hook 'evil-insert-state)
  (yas-global-mode)
  (general-spc
    "s" '(nil :which-key "snippets")
    "ss" 'yas-insert-snippet
    "sl" 'yas-describe-tables
    "se" 'yas-visit-snippet-file
    "sn" 'yas-new-snippet))

(use-package yasnippet-snippets)

;; Automatic determination of indent mode
(use-package dtrt-indent
  :diminish 'dtrt-indent-mode
  :hook (prog-mode . dtrt-indent-mode))

;; Start server if it isn't running
(use-package server
  :config (or (server-running-p) (server-mode)))

(use-package restclient)

(use-package alert
  :config
  (setq alert-default-style 'libnotify)
  ;; Alert me of finished compilations.
  (add-to-list 'compilation-finish-functions 'compilation-finished-alert))

(defun compilation-finished-alert (buffer status)
  "Alert that a compilation have finished in BUFFER and with STATUS."
  (alert (upcase status) :title "Compilation"))

(use-package slime-company)
(setq inferior-lisp-program "sbcl")
(slime-setup '(slime-company))
(setq slime-helper (expand-file-name "~/quicklisp/slime-helper.el"))
(if (file-exists-p slime-helper) (load slime-helper))

;;==============================================================================
;; Themes & UI/UX Stuff
;;==============================================================================

;; Which theme to use
(defvar ang/theme 'modus-operandi)

(use-package modus-operandi-theme :defer t)
(use-package modus-vivendi-theme :defer t)
(use-package solarized-theme :defer t)
(use-package spacemacs-theme :defer t)

(use-package rainbow-delimiters
  :hook (prog-mode . rainbow-delimiters-mode))

(use-package diff-hl
  :config
  (setq diff-hl-draw-borders nil)
  (global-diff-hl-mode)
  (add-hook 'magit-post-refresh-hook 'diff-hl-magit-post-refresh t))

;; This is needed when running emacs as a daemon, otherwise new frames will not
;; receive the theme.
(load-theme ang/theme t)
(if (daemonp)
    (add-hook 'after-make-frame-functions
              (lambda (frame)
                (when (eq (length (frame-list)) 2)
                  (progn
                    (select-frame frame)
                    (load-theme ang/theme t)))))
  (load-theme ang/theme t))

(setq default-frame-alist '((font . "Fira Code-9")))

(global-hl-line-mode)

;; start in fullscreen / maximized
(add-to-list 'default-frame-alist '(fullscreen . maximized))

;;==============================================================================
;; Other settings
;;==============================================================================

;; Try to not use tabs for indentation
(setq-default indent-tabs-mode nil)

;; y/n is enough for security
(defalias 'yes-or-no-p 'y-or-n-p)

;; Scroll to end of buffer
(setq compilation-scroll-output t)

(progn
  (setq dired-listing-switches "-alh")
  ;; Enable moving files/dirs between two open dired windows
  (setq dired-dwim-target t))

;; Some needed settings for mac
(when (memq window-system '(mac ns))
  (use-package exec-path-from-shell
    :config (exec-path-from-shell-initialize))
  ;; larger font size for osx
  (setq default-frame-alist '((font . "Fira Code-12")))
  )

;; Resize help buffers to fit content
(temp-buffer-resize-mode)
;; Reasonable width for man-pages
(setq Man-width 80)

(show-paren-mode)
(savehist-mode)
(save-place-mode)
(column-number-mode)

;; Collection of methods to move window focus in the given direction.
;; If it fails, instead let stumpwm move the focus.
(defun focus-right-or-stumpish ()
  (interactive)
  (condition-case nil
      (windmove-right)
    (error (call-process-shell-command "stumpish move-focus right"))))

(defun focus-left-or-stumpish ()
  (interactive)
  (condition-case nil
      (windmove-left)
    (error (call-process-shell-command "stumpish move-focus left"))))

(defun focus-down-or-stumpish ()
  (interactive)
  (condition-case nil
      (windmove-down)
    (error (call-process-shell-command "stumpish move-focus down"))))

(defun focus-up-or-stumpish ()
  (interactive)
  (condition-case nil
      (windmove-up)
    (error (call-process-shell-command "stumpish move-focus up"))))

(bind-key (kbd "<s-right>") #'focus-right-or-stumpish)
(bind-key (kbd "<s-left>") #'focus-left-or-stumpish)
(bind-key (kbd "<s-up>") #'focus-up-or-stumpish)
(bind-key (kbd "<s-down>") #'focus-down-or-stumpish)

;;==============================================================================
;; Languages
;;==============================================================================

(use-package groovy-mode
  :mode "\\.(gradle|groovy)\\'")
(use-package kotlin-mode
  :mode "\\.kt\\'"
  :config
  (setq kotlin-tab-width 4))
(use-package go-mode
  :mode "\\.go\\'")

(use-package elpy
  :hook (python-mode . elpy-mode)
  :config (setq python-prettify-symbols-alist nil))

(use-package clojure-mode
  :mode "\\.clj\\'")
(use-package cider)

;;==============================================================================
;; Org & Agenda
;;==============================================================================


(setq
 org-directory "~/org"
 org-default-notes-file (concat org-directory "/todo.org")
 )

(defun find-notes-file ()
  "Find and open the default org notes file in the current buffer."
  (interactive)
  (find-file org-default-notes-file))

(general-spc
  "o" '(nil :which-key "org")
  "oa" 'org-agenda
  "oc" 'counsel-org-capture
  "ol" 'org-store-link
  "or" 'org-refile
  "ot" 'org-todo-list
  "on" 'find-notes-file)

;; Load languages that are needed for babel
(org-babel-do-load-languages
 'org-babel-load-languages
 '((python . t)
   (shell . t)))

(setq
 ;; Don't ask for confirmation to execute source blocks
 org-confirm-babel-evaluate nil
 ;; Syntax highlighting in source blocks
 org-src-fontify-natively t
 ;; Please don't fuck up my tabs
 org-src-tab-acts-natively t
 ;; Search all agenda files and the current buffer up to a depth of 9
 ;; when refiling
 org-refile-targets '((nil :maxlevel . 9)
                      (org-agenda-files :maxlevel . 9))
 )

;; Enter insert mode when adding a new headline
(add-hook 'org-insert-heading-hook 'evil-insert-state)
(add-hook 'org-capture-mode-hook 'evil-insert-state)

;; My capture templates
(setq org-capture-templates
      '(("w" "Work" entry (file+headline org-default-notes-file "Work")
         "* TODO %?\n  %i%a")
        ("e" "Emacs" entry (file+headline org-default-notes-file "Emacs")
         "* TODO %?\n  %i%a")))

;;==============================================================================
;; Work stuff
;;==============================================================================


(defun ang/compile-from-project-root (cmd)
  "Run a 'compilation' from the project root, don't prompt, just run CMD."
  (let ((compilation-read-command nil))
    (projectile--run-project-cmd cmd nil
                                 :show-prompt nil
                                 :prompt-prefix ""
                                 :save-buffers t)))

(defhydra hydra-ofa (:color blue)
  "
Do various ofa-related stuffs.
--------------------------------------------------------------------------------
_b_: Build
_t_: Test
_r_: Run
_p_: Presubmit
_v_: Verify (presubmit + lint + test)
"
  ("b" (ang/compile-from-project-root "mobile/mobile/please build") nil)
  ("t" (ang/compile-from-project-root "mobile/mobile/please test") nil)
  ("r" (ang/compile-from-project-root "mobile/mobile/please run") nil)
  ("p" (ang/compile-from-project-root "mobile/mobile/tools/bin/presubmit.py") nil)
  ("v" (ang/compile-from-project-root "mobile/mobile/please check") nil)
  ("f" (ang/compile-from-project-root "python3 /hdd/code/scripts/format-all.py") nil))

(general-spc "q" 'hydra-ofa/body)

;; Try to load notmuch but don't be sad if it doesn't load
(when (require 'notmuch nil t)
  (general-spc "m" 'notmuch)
  (general-define-key
   :keymaps 'notmuch-show-mode-map
   :states 'normal
   "o" #'link-hint-open-link
   )
  )

;; Our avsc format is *actually* json
(add-to-list 'auto-mode-alist '("\\.avsc\\'" . javascript-mode))

;; Open a file in android studio
(defun open-in-android-studio ()
  "Open the current file in android studio."
  (interactive)
  (start-process "" nil "studio" (buffer-file-name)))
(general-spc "fo" #'open-in-android-studio)

;; Don't align java like absolute crap.
;; From: https://stackoverflow.com/questions/1365612/how-to-i-configure-emacs-in-java-mode-so-that-it-doesnt-automatically-align-met/1365821#1365821
(setq c-offsets-alist '((arglist-cont-nonempty . ++)
                        (arglist-intro . ++)
                        (case-label . +)
                        (statement-cont . ++)))

(setq frame-title-format "%b")

;; End:
;;; init.el ends here
