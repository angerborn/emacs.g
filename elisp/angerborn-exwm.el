;; -*- lexical-binding: t -*-
;; My exwm configuration
(use-package exwm)
(use-package framemove
  :config
  (setq framemove-hook-into-windmove t))
(require 'exwm)
(require 'exwm-config)
(require 'exwm-randr)
(add-hook 'exwm-randr-screen-change-hook
          (lambda ()
            (start-process-shell-command
             "xrandr" nil "xrandr --output DP-4 --left-of DP-6 --auto --output DP-6 --primary --auto")))
(setq exwm-randr-workspace-monitor-plist '(1 "DP-6" 0 "DP-4"))
(exwm-randr-enable)
(exwm-input-set-key (kbd "s-SPC") (lambda (command)
		         (interactive (list (read-shell-command "$ ")))
		         (start-process-shell-command command nil command)))
(exwm-input-set-key (kbd "s-h") #'windmove-left)
(exwm-input-set-key (kbd "s-j") #'windmove-down)
(exwm-input-set-key (kbd "s-k") #'windmove-up)
(exwm-input-set-key (kbd "s-l") #'windmove-right)
(exwm-input-set-key (kbd "s-o") #'evil-window-vsplit)
(exwm-input-set-key (kbd "s-u") #'evil-window-split)
(exwm-input-set-key (kbd "s-b") #'exwm-workspace-switch-to-buffer)
(defun angerborn/lock ()
  "Lock the screen by calling gnome-screensaver-command."
  (interactive)
  (start-process-shell-command "lock" nil "gnome-screensaver-command -l"))
(exwm-input-set-key (kbd "s-z") #'angerborn/lock)

;; Enable systray
(require 'exwm-systemtray)
(exwm-systemtray-enable)

(defun angerborn-exwm-config ()
  "My configuration of EXWM."
  ;; Set the initial workspace number.
  (unless (get 'exwm-workspace-number 'saved-value)
    (setq exwm-workspace-number 4))
  ;; Make title the buffer name, used to be class name, is it better?
  (add-hook 'exwm-update-class-hook
            (lambda ()
              (exwm-workspace-rename-buffer exwm-class-name)))
  ;; Global keybindings.
  (unless (get 'exwm-input-global-keys 'saved-value)
    (setq exwm-input-global-keys
          `(
            ;; 's-r': Reset (to line-mode).
            ([?\s-r] . exwm-reset)
            ;; 's-w': Switch workspace.
            ([?\s-w] . exwm-workspace-switch)
            ;; 's-&': Launch application.
            ([?\s-&] . (lambda (command)
		         (interactive (list (read-shell-command "$ ")))
		         (start-process-shell-command command nil command)))
            ;; 's-N': Switch to certain workspace.
            ,@(mapcar (lambda (i)
                        `(,(kbd (format "s-%d" i)) .
                          (lambda ()
                            (interactive)
                            (exwm-workspace-switch-create ,i))))
                      (number-sequence 0 9)))))
  ;; Line-editing shortcuts
  (unless (get 'exwm-input-simulation-keys 'saved-value)
    (setq exwm-input-simulation-keys
          '(([?\C-b] . [left])
            ([?\C-p] . [up])
            ([?\C-n] . [down])
            ([?\C-a] . [home])
            ([?\C-e] . [end])
            ([?\M-v] . [prior])
            ([?\C-v] . [next])
            ([?\C-d] . [delete]))))
  ;; Enable EXWM
  (exwm-enable)
  ;; Other configurations
  (fringe-mode 1))

(provide 'angerborn-exwm)
;;; angerborn-exwm ends here
